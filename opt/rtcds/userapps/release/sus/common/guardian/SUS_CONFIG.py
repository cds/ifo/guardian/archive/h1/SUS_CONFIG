from guardian import GuardState
from guardian import NodeManager
from time import sleep
import subprocess
import time

corner_suspensions = ['ZM1', 'ZM2',
                      'OPO', 'OFI',
                      'OMC', 'OM1', 'OM2', 'OM3',
                      'RM1', 'RM2',
                      'IM1', 'IM2', 'IM3', 'IM4',
                      'BS',
                      'SR2', 'SRM', 'SR3',
                      'PR2', 'PRM', 'PR3',
                      'MC1', 'MC2', 'MC3',
                      'ITMX', 'ITMY']

ex_suspensions = ['TMSX', 'ETMX']

ey_suspensions = ['TMSY', 'ETMY']

all_suspensions = corner_suspensions + ex_suspensions + ey_suspensions

quads = ['ETMX', 'ITMX', 'ETMY', 'ITMY']

# nominal operational state
nominal = 'ALL_ALIGNED'

# initial request on initialization
request = 'IDLE'

nodes = NodeManager(['SUS_{}'.format(sus) for sus in all_suspensions])

########################################
# Gen alignment state


def gen_align_state(state, sus_list, index=False, requestable=True):
    """State generator for a list of suspensions. Usage example:

    quads = ['ETMX','ITMX','ETMY','ITMY']
    QUADS_SAFE = gen_align_state('DAMPED', quads, index=40)

    """
    class ALIGN_STATE(GuardState):
        def main(self):
            for sus in sus_list:
                nodes['SUS_{}'.format(sus)] = state

        def run(self):
            if nodes.arrived:
                return True
    ALIGN_STATE.request = requestable
    if index:
        ALIGN_STATE.index = index

    return ALIGN_STATE


########################################
# States

class INIT(GuardState):
    def main(self):
        states_dict = {sus: nodes['SUS_{}'.format(sus)].STATE for sus in all_suspensions}
        # Check if all are the same, thatd be convenient
        if len(set(states_dict.values())) == 1:
            if all(state == 'ALIGNED' for state in states_dict.values()):
                return 'ALL_ALIGNED'
            elif all(state == 'DAMPED' for state in states_dict.values()):
                return 'ALL_DAMPED'
            elif all(state == 'SAFE' for state in states_dict.values()):
                return 'ALL_SAFE'
        # Dont want to go too into this, too many configs
        # If all else fails, sit in IDLE
        return 'IDLE'


class IDLE(GuardState):
    goto = True
    index = 1

    def run(self):
        return True


# All
ALL_SAFE = gen_align_state('SAFE', all_suspensions, index=10)

ALL_DAMPED = gen_align_state('DAMPED', all_suspensions, index=20)

ALL_ALIGNED = gen_align_state('ALIGNED', all_suspensions, index=30)
# Corner
CORNER_SAFE = gen_align_state('SAFE', corner_suspensions, index=40)

CORNER_DAMPED = gen_align_state('DAMPED', corner_suspensions, index=50)

CORNER_ALIGNED = gen_align_state('ALIGNED', corner_suspensions, index=60)
# EX
EX_SAFE = gen_align_state('SAFE', ex_suspensions, index=70)

EX_DAMPED = gen_align_state('DAMPED', ex_suspensions, index=80)

EX_ALIGNED = gen_align_state('ALIGNED', ex_suspensions, index=90)
# EY
EY_SAFE = gen_align_state('SAFE', ey_suspensions, index=100)

EY_DAMPED = gen_align_state('DAMPED', ey_suspensions, index=110)

EY_ALIGNED = gen_align_state('ALIGNED', ey_suspensions, index=120)


# OpLevs
class QUADS_OPLEV_ON(GuardState):
    index = 130
    request = True

    def main(self):
        sleep(0.5)
        for sus in quads:
            ezca['SUS-' + sus + '_L1_OLDAMP_P_TRAMP'] = 5
            ezca['SUS-' + sus + '_L1_OLDAMP_P_GAIN'] = 1

    def run(self):
        return True


class QUADS_OPLEV_OFF(GuardState):
    index = 140
    request = True

    def main(self):
        sleep(0.5)
        for sus in quads:
            ezca['SUS-' + sus + '_L1_OLDAMP_P_TRAMP'] = 5
            ezca['SUS-' + sus + '_L1_OLDAMP_P_GAIN'] = 0

    def run(self):
        return True

    
# WDs
class RESET_ALL_WD(GuardState):
    index = 150
    goto = True
    request = True

    def main(self):
        for sus in all_suspensions:
            if sus in ['OPO', 'OFI']:
                ezca['SUS-' + sus + '_WD_RESET'] = 1
            elif sus in ['ZM1', 'ZM2', 'RM1', 'RM2', 'OM1', 'OM2', 'OM3']:
                ezca['SUS-' + sus + '_M1_WD_RSET'] = 1
                ezca['SUS-HTTS_DACKILL_RESET'] = 1
            elif sus in ['IM1', 'IM2', 'IM3', 'IM4']:
                ezca['SUS-' + sus + '_M1_WD_RSET'] = 1
                ezca['SUS-IM_DACKILL_RESET'] = 1
            else:
                ezca['SUS-' + sus + '_WD_RESET'] = 1
                ezca['SUS-' + sus + '_DACKILL_RESET'] = 1
                ezca['IOP-SUS_' + sus + '_DACKILL_RESET'] = 1

    def run(self):
        return True


edges = [
    ('INIT', 'IDLE'),
    ('IDLE', 'ALL_SAFE'),
    ('IDLE', 'ALL_DAMPED'),
    ('IDLE', 'ALL_ALIGNED'),
    ('IDLE', 'CORNER_SAFE'),
    ('IDLE', 'CORNER_DAMPED'),
    ('IDLE', 'CORNER_ALIGNED'),
    ('IDLE', 'EX_SAFE'),
    ('IDLE', 'EX_DAMPED'),
    ('IDLE', 'EX_ALIGNED'),
    ('IDLE', 'EY_SAFE'),
    ('IDLE', 'EY_DAMPED'),
    ('IDLE', 'EY_ALIGNED'),
    ('IDLE', 'QUADS_OPLEV_OFF'),
    ('IDLE', 'QUADS_OPLEV_ON'),
]
